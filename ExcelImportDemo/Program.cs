﻿using ExcelImport;
using System;
using System.Collections.Generic;
using System.IO;

namespace ExcelImportDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("正在导出模板");

            var adv_helper = new Adv_ExcelImportHelper<Student>();
            adv_helper.ExportTemplate("student.xls");

            Console.WriteLine($"模板的路径：{Path.GetFullPath("student.xls")}");
        }
    }

    [ExcelSheet(SheetName = "学生表")]
    public class Student : ExcelData
    {
        [ExcelHeader("名称", Comment = "Name")]
        public string Name { get; set; }

        [ExcelHeader("年龄", Comment = "Age")]
        public string Age { get; set; }

        [ExcelHeader("性别", Comment = "Sex", ValidationData = new[] { "男", "女" })]
        public string Sex { get; set; }

        [ExcelHeader("其他")]
        public Dictionary<string, string> Others { get; set; }

        [ExcelHeader("亲属")]
        public Dictionary<string, Parent> Parents { get; set; }

        [ExcelHeader("非法集合")]
        public List<string> IllegalList { get; set; }

    }

    public class Parent
    {
        [ExcelHeader("关系")]
        public string Relations { get; set; }

        [ExcelHeader("姓名")]
        public string Name { get; set; }

        [ExcelHeader("年龄")]
        public string Age { get; set; }

        [ExcelHeader("备注")]
        public Dictionary<string, string> Remarks { get; set; }

    }
}
