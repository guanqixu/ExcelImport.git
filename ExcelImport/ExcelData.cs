﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExcelImport
{
    /// <summary>
    /// Excel 的数据
    /// </summary>
    public abstract class ExcelData
    {
        /// <summary>
        /// 标题属性
        /// </summary>
        private Dictionary<string, string> _header_property;

        /// <summary>
        /// 获取标题属性
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, string> GetHeaderProperty()
        {
            if (_header_property == null)
            {
                _header_property = new Dictionary<string, string>();
                var properties = GetType().GetProperties();
                foreach (var prop in properties)
                {
                    var attribute = Attribute.GetCustomAttribute(prop, typeof(ExcelHeaderAttribute));
                    if (attribute != null)
                    {
                        string name = (attribute as ExcelHeaderAttribute).Header;
                        _header_property.Add(name, prop.Name);
                    }
                }
            }
            return _header_property;
        }
    }

    /// <summary>
    /// Excel 标题
    /// </summary>
    public class ExcelHeaderAttribute : Attribute
    {
        /// <summary>
        /// 头
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// 注释
        /// </summary>
        /// <remarks>
        /// 若 <c>Comment</c> 不为空，则会在 excel 头部单元格中，增加注释
        /// </remarks>
        public string Comment { get; set; }

        /// <summary>
        /// 数据有效性
        /// </summary>
        /// <remarks>
        /// 若 ww<c>ValidationData</c> 不为空，则会在 excel 中产生下拉列表
        /// </remarks>
        public string[] ValidationData { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="header">头部的内容</param>
        public ExcelHeaderAttribute(string header)
        {
            Header = header;
        }
    }

    /// <summary>
    /// Excel 工作表
    /// </summary>
    public class ExcelSheetAttribute : Attribute
    {
        /// <summary>
        /// 工作表名称
        /// </summary>
        public string SheetName { get; set; }
    }

    /// <summary>
    /// Excel 列表项
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Obsolete("开发阶段尝试用的类，现在用 dictionary 代替了", true)]
    public class ExcelListItem<T>
    {
        /// <summary>
        /// 标题 列头名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }
    }

    /// <summary>
    /// excel 列头相关数据结构
    /// 
    /// 主要用于生成导出模板
    /// </summary>
    public class ExcelHeader : ICloneable
    {
        /// <summary>
        /// 列头文本
        /// </summary>
        public string HeaderText;

        /// <summary>
        /// 相关的属性
        /// </summary>
        public string PropertyName;

        /// <summary>
        /// 下方的列头集合
        /// </summary>
        public List<ExcelHeader> Children { get; set; } = new List<ExcelHeader>();

        /// <summary>
        /// 占多少个单元格宽度
        /// </summary>
        public int Width
        {
            get
            {
                if (Children.Count == 0)
                    return 1;
                else
                {
                    return Children.Sum(c => c.Width);
                }

            }
        }

        /// <summary>
        /// 整个列头（包括子列头），占多少个单元格高度
        /// </summary>
        public int Height
        {
            get
            {
                if (Children.Count == 0)
                    return 1;
                else
                {
                    return 1 + Children.Max(c => c.Height);
                }
            }
        }

        /// <summary>
        /// 起始单元格 列序号
        /// </summary>
        public int FirstCol { get; set; }

        /// <summary>
        /// 结束单元格 列序号
        /// </summary>
        public int LastCol { get; set; }

        /// <summary>
        /// 创建副本
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            var duplicate = new ExcelHeader()
            {
                HeaderText = HeaderText,
                PropertyName = PropertyName,
                FirstCol = FirstCol,
                LastCol = LastCol,
                Comment = Comment,
                ValidationData = ValidationData,
            };

            foreach (var child in Children)
            {
                duplicate.Children.Add(child.Clone() as ExcelHeader);
            }

            return duplicate;
        }

        /// <summary>
        /// 左对齐
        /// 
        /// 用于设置子项的起始单元格 列序号
        /// </summary>
        public void LeftJustified()
        {
            if (Children.Count >= 1)
            {
                Children[0].LeftJustified();
                Children[0].FirstCol = FirstCol;
                Children[0].LastCol = Children[0].FirstCol + Children[0].Width - 1;
            }

            for (int i = 1; i < Children.Count; i++)
            {
                Children[i].LeftJustified();
                Children[i].FirstCol = Children[i - 1].LastCol + 1;
                Children[i].LastCol = Children[i].FirstCol + Children[i].Width - 1;
            }
        }

        /// <summary>
        /// 注释
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 数据有效性
        /// </summary>
        public string[] ValidationData { get; set; }

    }


}
